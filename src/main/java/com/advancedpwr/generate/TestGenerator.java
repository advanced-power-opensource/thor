package com.advancedpwr.generate;

import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

public class TestGenerator
{
	
	protected File fieldSourceDirectory;
	
	public File getSourceDirectory()
	{
		if ( fieldSourceDirectory == null )
		{
			fieldSourceDirectory = new File("src/main/java");
		}
		return fieldSourceDirectory;
	}

	public void setSourceDirectory( File sourceDirectory )
	{
		fieldSourceDirectory = sourceDirectory;
	}
	
	public void setSourceDirectory( String sourceDirectory )
	{
		fieldSourceDirectory = new File(sourceDirectory);
	}

	public File getTestDirectory()
	{
		if ( fieldTestDirectory == null )
		{
			fieldTestDirectory = new File( "src/test/java");
		}
		return fieldTestDirectory;
	}

	public void setTestDirectory( File testDirectory )
	{
		fieldTestDirectory = testDirectory;
	}

	protected File fieldTestDirectory;
	
	public void testMakeAuto() throws Exception
	{
		File dir = getSourceDirectory();
		
		List<File> files = listJavaFiles( dir );
		for ( File file : files )
		{
			String filename = file.getAbsolutePath().replace( dir.getAbsolutePath() + File.separator, "" ).replace(".java", "" );
			String classname = filename.replace( File.separatorChar, '.' );
			System.out.println( classname );
			Class target = Class.forName( classname );
			if ( Modifier.isAbstract( target.getModifiers() ) )
			{
				continue;
			}
			Constructor[] constructors = target.getConstructors();
			for ( int i = 0; i < constructors.length; i++ )
			{
				if (constructors[i].getParameterCount() == 0)
				{
					System.out.println("Found no-arg constructor");
					Object object = constructors[i].newInstance( null );
					makeTest( object );
					break;
				}
			}

		}
	}
	
	protected List<File> listJavaFiles( File dir )
	{
		List<File> javaFiles = new ArrayList<>();
		listJavaFiles( dir, javaFiles );
		return javaFiles;
	}
	
	protected void listJavaFiles( File dir, List<File> list )
	{
		File[] listing = dir.listFiles();
		for ( File file : listing )
		{
			if ( file.isDirectory() )
			{
				listJavaFiles( file, list );
			}
			if ( file.isFile() && file.getName().endsWith( ".java" ) )
			{
				list.add( file );
			}
		}
	}

	public void makeTest( Object object )
	{
		TestWriter writer = new TestWriter();
		writer.setDestination( getTestDirectory() );
		writer.record( object );
	}
	
	public void makeTestFromClass( Class target ) throws Exception
	{
		if ( Modifier.isAbstract( target.getModifiers() ) )
		{
			System.out.println( target.getName() + " is abstract, skipping..." );
			return;
		}
		Constructor[] constructors = target.getConstructors();
		for ( int i = 0; i < constructors.length; i++ )
		{
			if (constructors[i].getParameterCount() == 0)
			{
				System.out.println("Found no-arg constructor");
				Object object = constructors[i].newInstance( null );
				makeTest( object );
				break;
			}
		}
	}

}
